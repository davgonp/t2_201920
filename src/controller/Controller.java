package controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import model.data_structures.Queue;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		Integer dato = null;
		Integer dato1 = null;
		Integer respuesta = null;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
			
				System.out.println("Archivo cargado");
				System.out.println("Numero de elementos " + modelo.cargar() + "\n---------");						
				break;

			case 2:
				System.out.println("--------- \nDar hora a ingresar: ");
				dato = lector.nextInt();
				Queue<Viaje> cola =modelo.darClusterDeViajePorHora(dato);
				System.out.println("El cluster de viajes con respecto a la hora:" + dato +" es:"+"\n---------");
				while ( cola.isEmpty())
				{    
					System.out.println("Hora: "+ cola.dequeue().getHod()+ "Zona de origen: "+ cola.dequeue().getSourceId()+ "Zona destino: " + cola.dequeue().getDstId()+ "Tiempo promedio: " + cola.dequeue().getMeanTravelTime()+ "\n---------");
				}
				break;

			case 3:
				System.out.println("--------- \nDar hora a ingresar: ");
				dato = lector.nextInt();
				System.out.println("--------- \nDar numero a ingresar: ");
				dato1 = lector.nextInt();
				Queue <Viaje> cola1= modelo.nUltimosViajes(dato1, dato);
				System.out.println( "los ultimos" + dato1+"de viajes con respecto a la hora "+ dato+ "son:" );						
				while ( cola1.isEmpty())
				{    
					System.out.println("Hora: "+ cola1.dequeue().getHod()+ "Zona de origen: "+ cola1.dequeue().getSourceId()+ "Zona destino: " + cola1.dequeue().getDstId()+ "Tiempo promedio: " + cola1.dequeue().getMeanTravelTime()+ "\n---------");
				}
				
				break;


			case 4: 
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;	

			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}

	
}
